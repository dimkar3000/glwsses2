#pragma once
#include "stack.h"
#include "heap.h"

#ifdef DEBUG
    #define DEBUG_COMMAND(x) x
#else
    #define DEBUG_COMMAND(x)
#endif


using byte = unsigned char;

namespace cpu {

    template<typename T>
    void jump(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i) {
        DEBUG_COMMAND(std::cout << std::hex << "JMP: " << i << std::endl);
        i = code[i+2] << 8 | code[i+1];
        return;
    }

    template<typename T>
    void jnz(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i) {
        int status = stack.pop();
        DEBUG_COMMAND(std::cout << "JNZ: " << std::hex << i << " Taken: "<< (status !=0) <<  std::endl);
        if (status != 0) {
            i = code[i+2] << 8 | code[i+1];
        }
        else{
            i+=3;
        }
        //std::cout << std::hex << (int) code[i+1] << std::endl;
        //std::cout << std::hex << (code[i+2]<< 8) << std::endl;
        return;
    }
    
    template<typename T>
    void dup(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i) {
        int a = code[i+1];
        DEBUG_COMMAND(std::cout << "DUP: " << a << std::endl);
        stack.Dup(a);
        i+=2;
        return;
    }

    template<typename T>
    void swap(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i) {
        int a = code[i+1];
        DEBUG_COMMAND(std::cout << "SWAP: " << a << std::endl);
        stack.swap(a);
        std::cout << "t" << std::endl;
        i+=2;
        return;
    }
    template<typename T>
    void drop(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i) {
        int droped = stack.pop();
        DEBUG_COMMAND(std::cout << "DROP: " << droped << std::endl);                
        i++;
        return;
    }
    template<typename T>
    void push4(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int number = code[i+4] << 24 | code[i+3] << 16 | code[i+2] << 8 | code[i+1] << 0;
        DEBUG_COMMAND(std::cout << "Pushing4: " << std::dec <<(uint32_t) number << std::endl);
        stack.push(number);
        //std::cout << std::hex << (code[i+1] << 24) << std::endl;
        //std::cout << std::hex << (code[i+2] << 16) << std::endl;
        //std::cout << std::hex << (code[i+3] << 8) <<std::endl;
        //std::cout << std::hex << (int)code[i+4] << std::endl;
        i += 5;
        return;
    }
    template<typename T>
    void push2(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int number = code[i+2] << 8 | code[i+1];
        DEBUG_COMMAND(std::cout << "Pushing2: " << number << std::endl);
        stack.push(number);
        i+= 3;
        return;
    }
    template<typename T>
    void push1(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int number = code[i+1];
        DEBUG_COMMAND(std::cout << "Pushing1: " << (char)number << "\n\tWith ASCII: " << number << std::endl);
        stack.push(number);
        i+=2;
        return;
    }
    template<typename T>
    void add(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop();
        int result = b + a;
        DEBUG_COMMAND(std::cout << "ADD: " << a << " + " << b << " = " << result << std::endl);
        stack.push(result);
        i++;

        return;
    }
    template<typename T>
    void sub(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop();
        int result = a - b;
        DEBUG_COMMAND(std::cout << "SUB: " << a << " - " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        //std::cin.get();
        return;
    }
    template<typename T>
    void mul(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop();
        int result = b * a;
        DEBUG_COMMAND(std::cout << "MUL: " << a << " * " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }
    template<typename T>
    void div(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop();
        int result = a / b;
        DEBUG_COMMAND(std::cout << "DIV: " << a << " / " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }
    template<typename T>
    void mod(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop(); 
        int result = a % b;
        DEBUG_COMMAND(std::cout << "MOD: " << a << " % " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }
    template<typename T>
    void eq(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop();
        int result = a == b ? 1 : 0;
        DEBUG_COMMAND(std::cout << "EQ: " << a << " == " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }
    template<typename T>
    void ne(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop();
        int result = a != b ? 1 : 0;
        DEBUG_COMMAND(std::cout << "NE: " << a << " != " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }
    template<typename T>
    void lt(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop();
        int result = a < b ? 1 : 0;
        DEBUG_COMMAND(std::cout << "LT: " << a << " < " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }
    template<typename T>
    void gt(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop();
        int result = a > b ? 1 : 0;
        DEBUG_COMMAND(std::cout << "GT: " << a << " > " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }
    template<typename T>
    void le(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop();
        int result = a <= b ? 1 : 0;
        DEBUG_COMMAND(std::cout << "LE: " << a << " <= " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }
    template<typename T>
    void ge(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop();
        int result = a >= b ? 1 : 0;
        DEBUG_COMMAND(std::cout << "GE: " << a << " >= " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }
    template<typename T>
    void notN(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int a = stack.pop();
        int result = a == 0;
        DEBUG_COMMAND(std::cout << "NOT: " << "not " << a << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }
    template<typename T>
    void andN(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int b = stack.pop();
        int a = stack.pop();
        int result = a && b  ? 1 : 0;
        DEBUG_COMMAND(std::cout << "AND: " << a << " && " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }
    template<typename T>
    void orN(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int a = stack.pop();
        int b = stack.pop();
        int result = a || b ? 1 : 0;
        DEBUG_COMMAND(std::cout << "OR: " << a << " || " << b << " = " << result << std::endl);
        stack.push(result);
        i++;
        return;
    }

    template<typename T>
    void halt(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i){
        DEBUG_COMMAND(std::cout << "HALT" << std::endl);
        //exit(0);
        return;
                
    }

    template<typename T>
    void cons(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {

        int b = stack.pop();
        int a = stack.pop();
        int key = heap.add(a,b);
        DEBUG_COMMAND(std::cout << "CONS: (" << a << "," << b << ") with key: " << key << std::endl;)
        stack.push(key);
        i++;
        return;
    }

    template<typename T>
    void hd(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        int key = stack.pop();
        int value = heap.get_head(key);
        DEBUG_COMMAND(std::cout << "HD: (key,value): (" <<key << ',' << value << ")" << std::endl;)
        stack.push(value);
        i++;
        return;
    }

    template<typename T>
    void tl(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    { 
        int key = stack.pop();
        int value = heap.get_tail(key);
        DEBUG_COMMAND(std::cout << "TL: (key,value): (" <<key << ',' << value << ")" << std::endl;)
        stack.push(value);
        i++;
        return;
    }

    template<typename T>
    void naf(std::vector<byte>& code,Stack<T>& stack, Heap<T>& heap, int& i)
    {
        throw;
    }
}