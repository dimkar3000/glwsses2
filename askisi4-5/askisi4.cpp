#include <iostream>
#include <chrono>
#include <iomanip>
#include <fstream>
#include <iterator>
#include <vector>
#include <ratio>

#include "stack.h"
#include "opcodes.h"
#include "heap.h"
#include "cpu.h"
//#define DEBUG
#ifdef DEBUG
    #define DEBUG_COMMAND(x) x
#else
    #define DEBUG_COMMAND(x)
#endif


#define STACK_SIZE 2 << 26
using byte = unsigned char;


void run(std::vector<byte>& code)
{
    Stack<int> stack(STACK_SIZE);
    Heap<int> heap(&stack);
    auto start = std::chrono::high_resolution_clock::now();
    int i = 0;
    while(true)
    {
        #ifdef DEBUG 
            std::cin.get();
        #endif
        Bytecode::Bytecode command = (Bytecode::Bytecode)code[i];
        DEBUG_COMMAND(std::cout << "Index: " << i << ", Command: " << (int) code[i] << std::endl);
        //c++;
        //if(c % 1000){
        //    std::cout << "Run " << c << "commands so far" << std::endl;
        //}
        //if(c == 100) break;
        switch (command)
        {
            case Bytecode::halt: 
            {
                cpu::halt(code,stack,heap,i);
                return;
            }
            case Bytecode::jump: 
            {
                cpu::jump(code,stack,heap,i);
                break;
            }
            case Bytecode::jnz: 
            {
                cpu::jnz(code,stack,heap,i);
                break;
            }
            case Bytecode::dup: 
            {
                cpu::dup(code,stack,heap,i);
                break;
            }
            case Bytecode::swap: 
            {
                cpu::swap(code,stack,heap,i);
                break;
            }
            case Bytecode::drop: 
            {
                cpu::drop(code,stack,heap,i);
                break;
            }
            case Bytecode::push4:
            {
                cpu::push4(code,stack,heap,i);
                break;
            }
            case Bytecode::push2:
            {
                cpu::push2(code,stack,heap,i);
                break;
            }
            case Bytecode::push1:
            {
                cpu::push1(code,stack,heap,i);
                break;
            }
            case Bytecode::add:
            {
                cpu::add(code,stack,heap,i);
                break;
            }
            case Bytecode::sub:
            {
                cpu::sub(code,stack,heap,i);
                break;
            }
            case Bytecode::mul:
            {
                cpu::mul(code,stack,heap,i);
                break;
            }
            case Bytecode::div:
            {
                cpu::div(code,stack,heap,i);
                break;
            }
            case Bytecode::mod:
            {
                cpu::mod(code,stack,heap,i);
                break;
            }
            case Bytecode::eq:
            {
                cpu::eq(code,stack,heap,i);
                break;
            }
            case Bytecode::ne:
            {
                cpu::ne(code,stack,heap,i);
                break;
            }
            case Bytecode::lt:
            {
                cpu::lt(code,stack,heap,i);
                break;
            }
            case Bytecode::gt:
            {
                cpu::gt(code,stack,heap,i);
                break;
            }
            case Bytecode::le:
            {
                cpu::le(code,stack,heap,i);
                break;
            }
            case Bytecode::ge:
            {
                cpu::ge(code,stack,heap,i);
                break;
            }
            case Bytecode::notN:
            {
                cpu::notN(code,stack,heap,i);
                break;
            }
            case Bytecode::andN:
            {
                cpu::andN(code,stack,heap,i);
                break;
            }
            case Bytecode::orN:
            {
                cpu::orN(code,stack,heap,i);
                break;
            }
            case Bytecode::input:
            {
                char c;
                std::cin.get(c);
                int a = c;
                DEBUG_COMMAND(std::cout << "INPUT: " << a << std::endl);
                stack.push(a);
                i++;
                break;
            }
            case Bytecode::output:
            {
                int a =  stack.pop();
                #ifndef DEBUG
                    std::cout <<(char) a << std::flush;
                #else
                    std::cout << "OUTPUT: " << (char)a << "\n\tAscii: " << a << std::endl;
                #endif
                i++;
                break;
            }
            case Bytecode::clock:
            {
                auto finish = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double, std::ratio<1,1>> int_sec = finish - start;
                std::cout << std::fixed << std::setprecision(6);
                #ifdef DEBUG
                    std::cout << "CLOCK: " << int_sec.count() << std::endl;
                #else 
                    std::cout << int_sec.count() << std::endl;
                #endif
                i++;
                break;
            }
            case Bytecode::cons:
            {
                cpu::cons(code,stack,heap,i);
                break;
            }
            case Bytecode::hd:
            {
                cpu::hd(code,stack,heap,i);
                break;
            }
            case Bytecode::tl:
            { 
                cpu::tl(code,stack,heap,i);
                break;
            }

            default: {
                std::cout << "Wrong bytecode: " << command << "\n";
                std::cout << std::flush;
                return;
                break;
            }
        }
    }
}

int main(int argc, char const *argv[])
{
    std::ios::sync_with_stdio(false);
    auto start = std::chrono::high_resolution_clock::now();
    if(argc != 2)
    {
        std::string n(argv[0]);
        std::size_t found = n.find_last_of("/\\");
        std::cout << "Usage: " << n.substr(found+1).c_str() << " FILE\n";
        return 0;
    }
    // Buffered Read of the code 
    std::ifstream input(argv[1], std::ios::binary);
    std::vector<byte> buffer(std::istreambuf_iterator<char>(input), {});
    std::cout << "File Size: "<< buffer.size() << " bytes" << std::endl;
    std::cout << "Program Output:\n" << std::endl; 
    run(buffer);
    
    
    std::cout << "\nExecution Finised" << std::endl;
    // Cleaning Up
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::ratio<1,1>> int_sec = finish - start;
    std::cout << std::fixed << std::setprecision(6);
    std::cout << "Execution Time:"<< int_sec.count()<< std::endl;
    return 0;
}
