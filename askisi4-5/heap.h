#pragma once

#include <unordered_map>
#include <sstream>
#include <iostream>

#define HEAP_SIZE 1 << 20

struct HeapException : public std::exception
{
	const char * what () const throw ()
    {
    	return "Wrong Key given.";
    }
};

template<typename T>
struct heap_element {
    T key;
    T first;
    T second;
    bool marked = false;

    heap_element<T>* next = nullptr;
};

template<typename T>
class Heap{
private:
    uint32_t key_base =0;
    heap_element<T>* data;
    heap_element<T>* free_list = nullptr;
    Stack<T>* stack;
public:

    Heap(Stack<T> *stack)
    {
        this->stack = stack;
        this->data = new heap_element<T>[HEAP_SIZE];
        init_heap();
    }

    T add(T first, T second)
    {
        if (free_list == nullptr) {      // out of memory, do GC
            for (int i = 0; i < HEAP_SIZE; i++)  // 1. clear mark bits
                data[i].marked = false;
            
            int stack_size = 0;
            T* root = stack->access(stack_size);
            for(int i=0; i < stack_size; i++){
                if(root[i] & (1 << 31))
                    mark(&data[root[i] & 0x3fffffff]);                          // 2. mark phase
            }
            

            free_list = nullptr;                 // 3. sweep phase
            for (int i = 0; i < HEAP_SIZE; i++)
                if (!data[i].marked)
                    add_to_free_list(&data[i]);
            if (free_list == nullptr){
                std::cout << "HEAP OUT" << std::endl;
                throw;
            }
        }
        heap_element<T>* p = free_list;
        free_list = free_list->next;
        p->first = first;
        p->second = second;
        return p->key;
    }

    T get_head(T key)
    {
        return data[key & 0x3fffffff].first;
    }

    T get_tail(T key)
    {
        return data[key & 0x3fffffff].second;
    }
  

private:
    void add_to_free_list(heap_element<T>* object)
    {
        object->next = free_list;
        free_list = object;
    }

    void init_heap() {
        for (int i = 0; i < HEAP_SIZE; i++){
            data[i].key = i | (1 << 31);
            add_to_free_list(&data[i]); 
        }
    }


    void mark(heap_element<T>* p) {  // set the mark bit on p and all its descendants
        if (p == nullptr || p->marked)
            return;
        p->marked = true;
        if(p->first & (1 << 31))
            mark(&data[p->first & 0x3fffffff]);
        if(p->second & (1 << 31))
            mark(&data[p->second & 0x3fffffff]);
    }

};