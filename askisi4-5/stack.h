#pragma once

#include <algorithm>
#include <stdexcept>
#include <iostream>



template<typename T>
class Stack {
private:
    T* data;
    int lenght;
    int top = -1;
    bool empty;
public:
    Stack(int size)
    {
        lenght = size;
        data = new T[size];
        empty = true;
    }

    void push(T element)
    {
        if(empty) empty = false;
        if(top == lenght) throw std::invalid_argument("Stack Overflowed");
        data[++top] = element;
        //std::cout << "STACK PUSH : " << element << " at: " << top - 1 << std::endl;

    }

    T pop()
    {
        if (empty || top < 0)  {
            std::cout.flush();
            throw std::invalid_argument("Stack Underflow");    
        }
        if (top == 0) {
            empty = true;
            return data[top];
        }
        return data[top--];
    }
    int size()
    {
        return lenght;
    }

    void Dup(int i)
    {
        //std::cout << "DUPED: " << data[top-i]<<std::endl;
        this->push(data[top-i]);
    }
    void swap(int i)
    {
        std::cout << "t1 " << i << std::endl;
        T temp = data[top];
        std::cout << "t2" << top - i << std::endl;
        data[top] = data[top-i];
        std::cout << "t3" << std::endl;
        data[top-i] = temp;
    }

    T* access(int& size)
    {
        size = top;
        return data;
    }
};