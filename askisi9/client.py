from bs4 import BeautifulSoup
import requests
import sys


# code origin: https://www.geeksforgeeks.org/minimum-number-deletions-make-string-palindrome/
def lps(str): 
    n = len(str) 
   
    L = [[0 for x in range(n)]for y in range(n)] 
   
    for i in range(n): 
        L[i][i] = 1
   
    for cl in range( 2, n+1): 
        for i in range(n - cl + 1): 
            j = i + cl - 1
            if (str[i] == str[j] and cl == 2): 
                L[i][j] = 2
            elif (str[i] == str[j]): 
                L[i][j] = L[i + 1][j - 1] + 2
            else: 
                L[i][j] = max(L[i][j - 1],L[i + 1][j]) 
   
    return L[0][n - 1] 
   
def minimumNumberOfDeletions( str): 
    n = len(str) 
    l = lps(str)    
    return (n - l) 

def game(site):
    s = requests.Session()
    # Initial get
    t = s.get(site);
    soup = BeautifulSoup(t.text,'html.parser')
    quest = soup.find(id='question').contents[0]
    n = minimumNumberOfDeletions(quest)

    result = ""
    i = 0
    while True:
        p = soup.find_all('p')
        l = p[-1].contents[1].strip(" ")[-1]
        print(f"Round {i+1}, length: {l}, {quest}")
        print(f"Answer: {n}")
        
        #post q
        data = {"answer":str(n),"submit":"Submit!"}
        a = s.post(site,data)
        soup = BeautifulSoup(a.text,'html.parser')

        # stop on game end 
        if soup.find('input',value="Play again!") is not None:
            break

        # Continue
        print(soup.find_all('p')[-1].contents[0])    
        data = {"again":'Continue!'}
        a = s.post(site,data)
        soup = BeautifulSoup(a.text,'html.parser')
        quest = soup.find(id='question').contents[0]
        n = minimumNumberOfDeletions(quest)
        i += 1


    r = soup.find_all('p')
    print(r[-2].contents[0].contents[0])
    print(r[-2].contents[1][2:])
    print(r[-1].contents[0])

# site = "https://courses.softlab.ntua.gr/pl2/2018b/exercises/palseq.php"
# site = "http://localhost:8000/index.php"

if __name__ == '__main__':
    if(len(sys.argv) != 2):
        print("Usage Example: ./client.py url")
    game(sys.argv[1])

