<?php
session_start();
// print_r($_POST);
function lps($str) {
    $n = strlen($str); 
  
    // Create a table to store 
    // results of subproblems 
    $L; 
  
    // Strings of length 1 
    // are palindrome of length 1 
    for ($i = 0; $i < $n; $i++) 
        $L[$i][$i] = 1; 
  
    // Build the table. Note that 
    // the lower diagonal values  
    // of table are useless and  
    // not filled in the process. 
    // c1 is length of substring 
    for ($cl = 2; $cl <= $n; $cl++) 
    { 
        for ( $i = 0; 
              $i < $n -$cl + 1;  
              $i++) 
        { 
            $j = $i + $cl - 1; 
            if ($str[$i] == $str[$j] &&  
                            $cl == 2) 
                $L[$i][$j] = 2; 
            else if ($str[$i] == $str[$j]) 
                $L[$i][$j] =  
                        $L[$i + 1][$j - 1] + 2; 
              
            else
                $L[$i][$j] = max($L[$i][$j - 1],  
                                $L[$i + 1][$j]); 
        } 
    } 
  
    // length of longest  
    // palindromic subseq 
    return $L[0][$n - 1]; 
}

// function to calculate minimum 
// number of deletions 
function minimumNumberOfDeletions($str) 
{ 
    $n = strlen($str); 
  
    // Find longest  
    // palindromic subsequence 
    $len = lps($str); 
  
    // After removing characters  
    // other than the lps, we get 
    // palindrome. 
    return ($n - $len); 
} 

if(!isset($_SESSION["Q"])){
    $_SESSION['Q'] = 0;
    $_SESSION['W'] = false;
    $_SESSION['S'] = 0;
    $_SESSION['A'] = false;
    $_SESSION['P'] = false;
}
$q = $_SESSION['Q'];
$won = $_SESSION['W'];
$result = $_SESSION['A'];
$state = $_SESSION['S'];
$answear = $_SESSION['A'];
$playing = $_SESSION['P'];

$max = 5;
$questions = array("aaa","bbb","ccc","dd","aa","cssss");


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if(!isset($_SESSION["Q"])){
        exit();
    }
    $playing = true;
    if(!isset($_SESSION["TIMESTAMP"])) 
    {
        $_SESSION["TIMESTAMP"] = microtime(true);
    }

    if($_POST["submit"] === "Submit!")
    {
        $state = 1;
        $result = minimumNumberOfDeletions($questions[$q]) == intval($_POST['answer']);
        if($result && $q == $max)
        {
            $won = true;
            $_SESSION["T"] = (microtime(true) - $_SESSION["TIMESTAMP"]) ;

        }
        if ($result && !$won)
        {
            $q += 1;
        }
        
    }
    else if ($_POST['again'] === 'Continue!')
    {
        $state = 0;
        if ($result && !$won)
        {
            $q += 1;
        }
    }
    else if ($_POST['again'] === 'Play Again')
    {
        $q = 0;
        $won = false;
        $state = 0;
        $answear = false;
        $playing = false;
    }

    $_SESSION['Q'] = $q;
    $_SESSION['W'] = $won;
    $_SESSION['S'] = $state;
    $_SESSION['A'] = $answear;
    $_SESSION['P'] = $playing;

}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Find the longest palindrome!</title>
<style type="text/css">
body,td,th {
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: x-large;
color: #CCCCCC;
}

body {
background-color: #333399;
}

.question { color: #FFCC33; }
.emph     { color: #99ee99; }
.alert    { color: #ee77aa; }

.right {
color: #33FF66;
font-weight: bold;
}
.wrong {
color: #FF3366;
font-weight: bold;
}

a:link    { color: #CCFFFF; }
a:visited { color: #CCFFFF; }

input {
background-color: #eeee66;
color: #333399;
}

code {
font-family: Consolas, "Andale Mono", "Courier New", monospace, sans-serif;
color: #eeee99;
font-size: 120%;
}

span.removed {
color: #ff9977;
text-decoration: underline red;
}

code.block {
background-color: #66eeee;
color: #993333;
overflow-wrap: break-word;
display: block;
border: 1px solid black;
padding: 8px;
width: 95%;
line-height: 1em;
margin-top: 0.25em;
margin-bottom: 0.25em;
}

input.box {
overflow-wrap: break-word;
font-family: Consolas, "Andale Mono", "Courier New", monospace, sans-serif;
font-size: 120%;
color: #333333;
border: 1px solid black;
padding: 8px;
}

input.button {
font-size: 120%;
background-color: #99ee99;
color: #333399;
border: 1px solid black;
padding: 8px;
}


</style>
</head>
<body>
<h1>Find the longest palindrome!</h1>

<p>I\'ll give you a string of (up to 1000) letters
and I need you to do one simple thing:
</p>
<p>Find the <span class="emph">least</span> possible number of letters that,
if removed from the given string, what remains is a
<span class="emph">palindrome</span>.
</p>
<blockquote>
<p>For example, given the string:
<code>bbccaddabaddacaaacdb</code>
the correct answer is <span class="emph">5</span>.
</p>
<p>If one removes these five underlined letters:
<code>b<span class="removed">b</span>ccaddabaddac<span class="removed">aaa</span>c<span class="removed">d</span>b</code>
then the remaining string:
<code>bccaddabaddaccb</code>
is indeed a palindrome.  It is not possible to obtain a
palindrome by removing fewer than five letters.
</p>
</blockquote>

<hr />

<p><span class="question">Question <?php echo $_SESSION["Q"] + 1 ?></span>:
length <?php echo strlen($questions[$_SESSION["Q"]]) ?>  <code class="block" id="question"><?php echo $questions[$_SESSION["Q"]];?></code>
</p>


<?php if($state==0) {?>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" id="f" name="f" method="post">
What is the least number of characters you need to remove?
<table border="0" cellspacing="3">
<tr>
<td><input type="text" class="box" name="answer" id="answer" autofocus /></td>
<td><input type="submit" class="button" name="submit" id="submit" value="Submit!" /></td>
</tr>
</table>
</form>

<?php } else if($state==1) {?>

    
    <?php   if($won == true) echo "<p><span class=\"congratulations\"> Congratulations!</span>\r\nYou answered all questions!</p><p>It took you ". number_format($_SESSION["T"],3) ." seconds\r\n"; ?>
    
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" id="r" name="r" method="post">
    <input class="button" type="submit" name="again" id="again" value="<?php if ($_SESSION['W']) echo 'Play again!'; else echo 'Continue!'?>" autofocus="">
    </form>

    <?php if($result == true && !$won) echo "<p class=\"right\">Right! :-)</p>";
          else if($_SESSION["P"] == true and $state != 0 and !$won) echo "<p class=\"wrong\">Try again</p>";?>

<?php }?>
</p>
</body>
</html>
