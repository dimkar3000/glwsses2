{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE BangPatterns #-}

import Data.Array.MArray 
import Control.Monad.ST
import Data.Foldable
import Data.Array.ST
import Data.STRef

findPoli :: Int -> [Char] -> Int
findPoli _ [] = 0
findPoli n s = let 
    work = do
        input <- newListArray (0, n-1) s :: ST s (STArray s Int Char)
        prev2Ref <- newSTRef =<< (newArray (0, n-1) 0 :: ST s (STUArray s Int Int))
        prev1Ref <- newSTRef =<< (newArray (0, n-1) 1 :: ST s (STUArray s Int Int))
        for_ [1..n-1] $ \d -> do
            prev2 <- readSTRef prev2Ref
            prev1 <- readSTRef prev1Ref
            res <- newArray_ (0, n-1) 
            for_ [0..n-1-d] $ \i -> do
                c1 <- readArray input i
                c2 <- readArray input (i+d)
                if (c1 == c2) then do
                    x1 <- readArray prev1 (i+1)
                    x <- readArray prev1 i
                    writeArray res i ((x1 + x + 1) `mod` 20130401)
                else do
                    x1 <- readArray prev1 (i+1)
                    x <- readArray prev1 i
                    y1 <- readArray prev2 (i+1)
                    writeArray res i ((x1 + x -y1) `mod` 20130401)
            writeSTRef prev2Ref prev1
            writeSTRef prev1Ref res
        final <- (flip readArray 0) =<< readSTRef prev1Ref
        pure final
    in runST $ work

readN :: IO Int
readN = readLn

main = do
    n <- readN
    s <- getLine
    print $ findPoli n s