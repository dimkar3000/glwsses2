{-# LANGUAGE MultiWayIf #-}
import Control.Monad.ST
import Data.Foldable
import Data.Array.ST
import Data.STRef   

poliFindDirt :: Int -> [Char] -> Int
poliFindDirt n [] = 0
poliFindDirt n s = runST $ job where 
    job = do
        characters <- newListArray (0, n-1) s :: ST s (STArray s Int Char)
        row0 <- newSTRef =<< (newArray (0, n-1) 0 :: ST s (STUArray s Int Int))
        row1 <- newSTRef =<< (newArray (0, n-1) 1 :: ST s (STUArray s Int Int))
        for_ [1..n-1] $ \d -> do
            prev2 <- readSTRef row0
            prev1 <- readSTRef row1
            res <- newArray_ (0, n-1) 
            for_ [0..n-1-d] $ \i -> do
                f <- readArray characters i
                s <- readArray characters (i+d)
                x1 <- readArray prev1 (i+1)
                x <- readArray prev1 i     
                if 
                    | f == s -> do
                        writeArray res i ((x1 + x + 1) `mod` 20130401)
                    | otherwise -> do
                        y1 <- readArray prev2 (i+1)
                        writeArray res i ((x1 + x -y1) `mod` 20130401)
            writeSTRef row0 prev1
            writeSTRef row1 res
        fin <- (flip readArray 0) =<< readSTRef row1
        pure fin
        

readN :: IO Int
readN = do
    n <- getLine
    return (read n)

main = do
    n <- readN
    s <- getLine
    print  (poliFindDirt n s)