paliFind :: Int -> [Char] -> [[Int]]
paliFind n (h:hs) = let
    row0 = replicate n 0
    row1 = replicate n 1
    
    mapZip5 f (a:as) (b:bs) (c:cs) (d:ds) (e:es) = z `seq` (z :) $! mapZip5 f as bs cs ds es where z = f a b c d e
    mapZip5 f _ _ _ _ _ = []
    
    step (_:pre2Tail) (p:ph) (g:gh) = z `seq` z : step (p:ph) z gh where 
        z = mapZip5 work ph (p:ph) pre2Tail (h:hs) (g:gh) 
        work x1 x y1 c c_ = x1 `seq` x `seq` y1 `seq` c `seq` c_ `seq` (if c == c_ then  (x1 + x + 1) else (x1 + x - y1)) `mod` 20130401
    in 
        row0 : row1 : step row0 row1 hs

readN :: IO Int
readN = readLn


main = do
    n <- readN
    s <- getLine
    print $ (paliFind n s) !! n !! 0
    

    
    
