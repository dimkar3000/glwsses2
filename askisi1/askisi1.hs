{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE BangPatterns #-}

import Data.Array.MArray 
import Control.Monad.ST
import Data.Foldable
import Data.Array.ST
import Data.STRef
    
cpals1 :: (Ord a) => [a] -> [[Int]]
cpals1 s = base1 : base2 : step base1 base2 (tail s)
    where base1 = replicate n 0 
          base2 = replicate n 1
          n = length s
          step prev2 prev s_ = let !z = sZipWith5 work (tail prev) prev (tail prev2) s s_ in z : step prev z (tail s_)
          sZipWith5 f (a:as) (b:bs) (c:cs) (d:ds) (e:es) = let !z = f a b c d e in (z :) $! sZipWith5 f as bs cs ds es
          sZipWith5 f _ _ _ _ _ = []
          work !x1 !x !y1 !c !c_
              | c == c_ = (x1 + x + 1) `mod` 20130401 
              | otherwise = (x1 + x - y1) `mod` 20130401

cpals2 :: forall a. Eq a => [a] -> Int
cpals2 [] = 0
cpals2 s = runST $ do
        input <- newListArray (0, n-1) s :: ST s (STArray s Int a)
        prev2Ref <- newSTRef =<< (newArray (0, n-1) 0 :: ST s (STUArray s Int Int))
        prev1Ref <- newSTRef =<< (newArray (0, n-1) 1 :: ST s (STUArray s Int Int))
        for_ [1..n-1] $ \d -> do
            prev2 <- readSTRef prev2Ref
            prev1 <- readSTRef prev1Ref
            res <- newArray_ (0, n-1) 
            for_ [0..n-1-d] $ \i -> do
                c1 <- readArray input i
                c2 <- readArray input (i+d)
                if (c1 == c2) then do
                    x1 <- readArray prev1 (i+1)
                    x <- readArray prev1 i
                    writeArray res i ((x1 + x + 1) `mod` 20130401)
                else do
                    x1 <- readArray prev1 (i+1)
                    x <- readArray prev1 i
                    y1 <- readArray prev2 (i+1)
                    writeArray res i ((x1 + x -y1) `mod` 20130401)
            writeSTRef prev2Ref prev1
            writeSTRef prev1Ref res
        final <- (flip readArray 0) =<< readSTRef prev1Ref
        pure final
        where n = length s

readN :: IO Int
readN = readLn

main = do
    n <- readN
    s <- getLine
    print $ cpals1 s !! n !! 0
    print $ cpals2 s
    

    
    
