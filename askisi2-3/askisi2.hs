module ASKISI2 where 

{- Part 1 -}
data Tree a = T a [Tree a]  deriving (Eq, Show)

foldTree :: (a -> [b] -> b) -> Tree a -> b
foldTree f (T a cs) = f a (fmap (foldTree f) cs)

{- Part 2 -}
sizeTree :: Num b => Tree a -> b
sizeTree = foldTree (\_ s -> 1 + foldr (+) 0 s)

heightTree :: (Ord b, Num b) => Tree a -> b 
heightTree = foldTree (\_ xs -> if xs == [] then 1 else maximum xs + 1)

sumTree :: Num a => Tree a -> a
sumTree = foldTree (\x xs -> x + sum xs)

maxTree :: Ord a => Tree a -> a
maxTree = let 
    go x [] = x
    go x xs = max x $ maximum xs
    in foldTree go

inTree :: Eq a => a -> Tree a -> Bool
inTree t = foldTree (\x xs -> if or xs then True else x == t)

nodes :: Tree a -> [a]
nodes = foldTree (\x xs -> x : concat xs)

countTree :: (a -> Bool) -> Tree a -> Integer
countTree f = foldTree (\x xs -> if f x then 1 + sum xs else sum xs)

leaves :: Tree a -> [a]
leaves = let 
    go x [] = [x]
    go x xs = concat xs
    in foldTree go

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f = foldTree (\x xs -> T (f x) xs)

trimTree :: Int -> Tree a -> Tree a
trimTree n (T a cs) = if n == 0 then T a [] else T a $ map (trimTree $ n-1) cs

path :: [Int] -> Tree a -> a
path [] (T a _) = a
path (i : is) (T a cs) = path is (cs !! i)

