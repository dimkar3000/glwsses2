module Main where

    import Test.QuickCheck
    import Text.Show.Functions
    import Data.Foldable
    import Control.Applicative
    import Control.Monad
    import Data.Ratio
    import Numeric.Natural
    
{- Part 1 -}
    data Tree a = T a [Tree a]  deriving (Eq, Show)

    foldTree :: (a -> [b] -> b) -> Tree a -> b
    foldTree f (T a cs) = f a (fmap (foldTree f) cs)

{- Part 2 -}
    sizeTree :: Num b => Tree a -> b
    sizeTree = foldTree (\_ s -> 1 + foldr (+) 0 s)

    heightTree :: (Ord b, Num b) => Tree a -> b 
    heightTree = foldTree (\_ xs -> if xs == [] then 1 else maximum xs + 1)

    sumTree :: Num a => Tree a -> a
    sumTree = foldTree (\x xs -> x + sum xs)

    maxTree :: Ord a => Tree a -> a
    maxTree = let 
        go x [] = x
        go x xs = max x $ maximum xs
        in foldTree go

    inTree :: Eq a => a -> Tree a -> Bool
    inTree t = foldTree (\x xs -> if or xs then True else x == t)

    nodes :: Tree a -> [a]
    nodes = foldTree (\x xs -> x : concat xs)
    
    countTree :: (a -> Bool) -> Tree a -> Integer
    countTree f = foldTree (\x xs -> if f x then 1 + sum xs else sum xs)
    
    leaves :: Tree a -> [a]
    leaves = let 
        go x [] = [x]
        go x xs = concat xs
        in foldTree go
    
    mapTree :: (a -> b) -> Tree a -> Tree b
    mapTree f = foldTree (\x xs -> T (f x) xs)
    
    trimTree :: Int -> Tree a -> Tree a
    trimTree n (T a cs) = if n == 0 then T a [] else T a $ map (trimTree $ n-1) cs
    
    path :: [Int] -> Tree a -> a
    path [] (T a _) = a
    path (i : is) (T a cs) = path is (cs !! i)

    {- Askisi 3 -}

    {- Part 1-}
 
    instance Arbitrary a => Arbitrary (Tree a) where
        arbitrary = sized generateTree where 
            generateTree 0 = fmap T arbitrary <*> pure []
            generateTree l = do
                n <- choose (0, l-1)
                i <- arbitrary
                cs <- vectorOf n (generateTree ( n `div` 2))
                pure (T i cs)

    {- Part 2 -}
    heightLessThanSize :: Tree Integer -> Bool
    heightLessThanSize t =  let h = heightTree t
                                s = sizeTree t
                            in h >= 0 && h <= s
     
    maxOfTree :: Tree Integer -> Bool
    maxOfTree t = inTree (maxTree t) t
     
    allForTree :: Tree Integer -> Bool
    allForTree t = let n = nodes t
                     in all (`inTree` t) n
     
    countGreaterThanSize :: Tree Integer -> (Integer -> Bool) -> Bool
    countGreaterThanSize t f = let c = countTree f t
                     in c >= 0 && c <= (sizeTree t)
     
    nodescountGreaterThanSize :: Tree Integer -> Bool
    nodescountGreaterThanSize t = (length $ nodes t) == (sizeTree t)
     
    mapPreservesHeightAndSize :: Tree Integer -> (Integer -> Integer) -> Bool
    mapPreservesHeightAndSize t f = let m = mapTree f t
                                   in (sizeTree t) == (sizeTree m) && (heightTree t) == (heightTree m)
     
    mappedInMappedTree :: Tree Integer -> (Integer -> Integer) -> Bool
    mappedInMappedTree t f = flip all (nodes t) $ \n -> inTree (f n) (mapTree f t)
     
    mapAndMapTree :: Tree Integer -> (Integer -> Integer) -> Bool
    mapAndMapTree t f = flip all [nodes, leaves] $ \g -> (map f . g) t == (g . mapTree f) t
    {- Part 3 -}
    bird :: Tree Rational
    bird = T 1 [(mapTree ((1/).(+1)) bird), mapTree ((+1).(1/)) bird]
     
    followPathBirdSameWithTrimBird :: [Bool] -> Bool
    followPathBirdSameWithTrimBird bp = let n = length p
                                            p = map fromEnum bp
                                            trimmed = trimTree n bird
                                         in path p bird == path p trimmed
     
    naturalInBird :: Positive Integer -> Bool
    naturalInBird (Positive n) = let p = take (fromInteger n - 1) $ cycle [1, 0]
                       in n % 1 == path p bird
     
     
    denominatorIsFib :: Positive Integer -> Bool
    denominatorIsFib (Positive n) = let p = replicate (fromInteger n-1) 0
                                        fibs = 0 : 1 : zipWith (+) fibs (tail fibs)
                                        fib = fibs !! (fromInteger n+1)
                                     in denominator (path p bird) == fib
     
    findBird :: Positive Rational -> [Int]
    findBird (Positive n) = map fromEnum $ reverse $ go n bird 0 []
        where go n (T a [l, r]) h p 
                | n == a = p
                | h `mod` 2 == 0 && n < a = go n l (h+1) (False : p)
                | h `mod` 2 == 0 && n > a = go n r (h+1) (True : p)
                | h `mod` 2 /= 0 && n < a = go n r (h+1) (True : p)
                | h `mod` 2 /= 0 && n > a = go n l (h+1) (False : p)
     
    positiveRationalInBird :: Positive Rational -> Bool
    positiveRationalInBird (Positive n) = let p = findBird (Positive n)
                                           in path p bird == n
    {- Τα tests είναι γραμένα με την σειρά που λέει η άσκηση-}
    tests = 
        [counterexample "Test 1: heightLessThanSize" heightLessThanSize
        ,counterexample "Test 2: maxOfTree" maxOfTree
        ,counterexample "Test 3: allForTree" allForTree
        ,counterexample "Test 4: countGreaterThanSize" countGreaterThanSize
        ,counterexample "Test 5: nodescountGreaterThanSize" nodescountGreaterThanSize
        ,counterexample "Test 6: mapPreservesHeightAndSize" mapPreservesHeightAndSize
        ,counterexample "Test 7: mappedInMappedTree" mappedInMappedTree
        ,counterexample "Test 8: mapAndMapTree" mapAndMapTree
        ,counterexample "Test 9: followPathBirdSameWithTrimBird" followPathBirdSameWithTrimBird
        ,counterexample "Test 10: naturalInBird" naturalInBird
        ,counterexample "Test 11: denominatorIsFib" denominatorIsFib
        ,counterexample "Test 12: positiveRationalInBird" positiveRationalInBird]
    main = quickCheck $ conjoin $ tests  
            
     